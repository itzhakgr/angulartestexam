import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { environment } from './../environments/environment';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { UsersService } from "./users.service";
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RouterModule } from '@angular/router';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { FireUsersComponent } from './fire-users/fire-users.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { UpdateFormComponent } from './users/update-form/update-form.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UsersFormComponent,
    FireUsersComponent,
    UpdateFormComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path: '', component: UsersComponent},  //default - localhost:4200 - homepage
      {path: 'users', component: UsersComponent},
      {path: 'fireusers', component: FireUsersComponent},
      {path:'users-firebase', component:FireUsersComponent},
      {path:'update-form/:id', component:UpdateFormComponent},
      {path:'login', component:LoginComponent},
      {path: '**', component: NotFoundComponent}//להשאיר בכל מקרה
    ])
  ],
  providers: [UsersService,HttpModule],
  bootstrap: [AppComponent]
})
export class AppModule { }

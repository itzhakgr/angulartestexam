import { UsersService } from '../users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'fire-users',
  templateUrl: './fire-users.component.html',
  styleUrls: ['./fire-users.component.css']
})

export class FireUsersComponent implements OnInit {

  users;

  constructor(private service:UsersService) { }

  ngOnInit() {
    this.service.getUsersFire().subscribe(users =>{
      this.users = users;
      console.log(this.users);
    });
  }

}
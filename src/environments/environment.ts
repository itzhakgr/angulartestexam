// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment =
{
	production: true,
	url: 'http://localhost/angular/slim/',
	firebase:
	{
		apiKey: "AIzaSyDr7CGjbhd7JvCUG0fT73gqSzPQQyMI9XU",
    	authDomain: "moeda-2e359.firebaseapp.com",
    	databaseURL: "https://moeda-2e359.firebaseio.com",
    	projectId: "moeda-2e359",
    	storageBucket: "moeda-2e359.appspot.com",
    	messagingSenderId: "731781654293"
	}
};

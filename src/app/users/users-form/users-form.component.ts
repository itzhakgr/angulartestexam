import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UsersService } from '../../users.service';

@Component({
  selector: 'users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.css']
})
export class UsersFormComponent implements OnInit {

  // Adding emitters
  @Output() addUser:EventEmitter <any> = new EventEmitter <any>();
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>();
  // Instance Variables
  service:UsersService;
  //Form Builder
  userForm = new FormGroup({
      name:new FormControl(),
      phonenum:new FormControl(),
  });  



  constructor(service:UsersService, private formBuilder:FormBuilder) { 
  	this.service = service;
    console.log('From Constructed');
  }

  sendData()
  {
    this.addUser.emit(this.userForm.value.name);
    this.service.postUser(this.userForm.value).subscribe(
      response =>{        
        console.log(response)
        this.addUserPs.emit();
      }
    )
  }



  ngOnInit()
  {
		/*this.userForm = this.formBuilder.group({
	      name:  [null, [Validators.required]],
	      phonenum: [null, Validators.required],
      });	   */
  }
}

import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users.service";

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit
{
  users;
  usersKeys=[];
  constructor(private service:UsersService)
  {
    service.getUsers().subscribe(response=>
    {
      //console.log(response);
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
    //let service = new UsersService();
    })
  }
    deleteUser(key){
    console.log(key);      
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1);
    //delete from server
    this.service.deleteUser(key).subscribe(
    response=> console.log(response));
  }

  updateUser(id){
    this.service.getUser(id).subscribe(response=>{
    this.users  = response.json();      
    });   
  }
  /*optimisticAdd(name){
    var newKey =  this. usersKeys[this.usersKeys.length-1] +1;
    var newMessageObject ={};
    newMessageObject['name'] = name;
    this.users[newKey] = newMessageObject;
    this.usersKeys = Object.keys(this.users);
  }
  pasemisticAdd(){
  this.service.getUsers().subscribe(response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);      
    });      
  }*/

   optimisticAdd(name)
   {
    var newKey = this.usersKeys[this.usersKeys.length-1]+1;
    console.log(this.usersKeys.length);
    console.log(this.usersKeys);
    var newUserObject = {};
    newUserObject['name'] = name;
    console.log(newUserObject);
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);
    console.log(this.users);
    console.log(event,newKey);
  }
  pessimiaticAdd()
  {
    this.service.getUsers().subscribe(response => {
      this.users =  response.json();
      this.usersKeys = Object.keys(this.users);
    });   
  }

  ngOnInit()
  {
  }

}

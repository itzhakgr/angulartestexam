import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from './../environments/environment';
import {AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class UsersService
{
  getUsers()
  {
    let token='?token='+localStorage.getItem('token');
    return this.http.get('http://localhost/angular/slim/users'+token);
  }

  getUsersFire()
  {
    return this.db.list('/users').valueChanges();    
  }
  postUser(data)
  {
    let token = localStorage.getItem('token');
    let params = new HttpParams().append('name',data.name).append('phonenum',data.phonenum).append('token',token);      
    let options =  {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'    
      })
    }      
    return this.http.post('http://localhost/angular/slim/users', params.toString(), options);
  }
  deleteUser(key)
  {
    let token=localStorage.getItem('token');
    console.log(key);  
    console.log(token);
    return this.http.delete('http://localhost/angular/slim/users/:'+ key + '/:' + token);
  }

  getUser(key){  
     return this.http.get('http://localhost/angular/slim/users/'+ key);
  }
  updateUser(data,key){       
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })   
    };

    let params = new HttpParams().append('name',data.name).append('phonenum',data.phonenum);  

    return this.http.put('http://localhost/angular/slim/users/'+key,params.toString(), options);      
  }
  login(credentials){ 
   let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('user',credentials.user).append('password',credentials.password);
    return this.http.post('http://localhost/angular/slim/auth',params.toString(),options).map(response=>{
      let token = response.json().token;
      if(token) localStorage.setItem('token',token);
    });
  }

  constructor(private http:Http, private db:AngularFireDatabase) { 
    //this.http = http;
    /*let service = new UsersService();
    this.users = service.getUsers(); */ }



}
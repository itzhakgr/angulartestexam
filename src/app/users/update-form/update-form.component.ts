import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Http,Headers } from '@angular/http';
import { HttpParams, HttpHeaders } from '@angular/common/http';
import { UsersService } from '../../users.service';

@Component({
  selector: 'update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {

  // Adding emitters
  @Output() addUser:EventEmitter <any> = new EventEmitter <any>();
  @Output() addUserPs:EventEmitter <any> = new EventEmitter <any>();
  // Instance Variables
  user;
  service:UsersService;
  //Form Builder
  usrform = new FormGroup({
      name:new FormControl(),
      phonenum:new FormControl(),
  });  
    constructor(private route: ActivatedRoute ,service: UsersService, private router: Router) {
    this.service = service;
  }
/* //MATAN CODE
  constructor(service:UsersService, private formBuilder:FormBuilder, private route: ActivatedRoute) {   	    
    this.service = service;    
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = +params.get('id'); // This line converts id from string into num      
      this.service.getUser(this.id).subscribe(response=>{
        this.user = response.json();                                
      });      
    });
  }*/

  sendData(){
  this.addUser.emit(this.usrform.value.name);
  console.log(this.usrform.value);
  this.route.paramMap.subscribe(params=>{
    let id = params.get('id');
    this.service.updateUser(this.usrform.value, id).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
        this.router.navigateByUrl("/");//return to main page
      }
    );
  })
}
 ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
    })
  }

}